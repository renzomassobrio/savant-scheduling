#!/bin/bash

# models-128x4-hihi-min-4000
# models-128x4-hilo-min-4000
# models-12x4-hihi-min-4000
# models-12x4-hilo-min-4000
# models-512x16-hihi-min-16000
# models-512x16-hilo-min-16000

#source /opt/intel/bin/compilervars.sh intel64
for t in {1..272}
do
        echo $t
	for e in {1..60}
	do
		#{ time ./BL 12 4 new_c_12x4_hihi_1.dat savantSched/models-12x4-hihi-min-4000 $t ; } > salidas/new_c_12x4_hihi_1.sol.$t.$e 2>&1;
                #{ time ./BL 12 4 new_c_12x4_hilo_1.dat savantSched/models-12x4-hilo-min-4000 $t ; } > salidas/new_c_12x4_hilo_1.sol.$t.$e 2>&1;
                #{ time ./BL 128 4 new_c_128x4_hihi_1.dat savantSched/models-128x4-hihi-min-4000 $t ; } > salidas/new_c_128x4_hihi_1.sol.$t.$e 2>&1;
                #{ time ./BL 128 4 new_c_128x4_hilo_1.dat savantSched/models-128x4-hilo-min-4000 $t ; } > salidas/new_c_128x4_hilo_1.sol.$t.$e 2>&1;
                { time ./BL 512 16 new_c_512x16_hihi_1.dat savantSched/models-512x16-hihi-min-16000 $t ; } > salidas/new_c_512x16_hihi_1.sol.$t.$e 2>&1;
                #{ time ./BL 512 16 new_c_512x16_hilo_1.dat savantSched/models-512x16-hilo-min-16000 $t ; } > salidas/new_c_512x16_hilo_1.sol.$t.$e 2>&1;
                { time ./BL 4096 16 scale_c_4096x16_hihi_1.dat savantSched/models-512x16-hihi-min-16000 $t ; } > salidas/scale_c_4096x16_hihi_1.sol.$t.$e 2>&1;
                #{ time ./BL 4096 16 scale_c_4096x16_hilo_1.dat savantSched/models-512x16-hilo-min-16000 $t ; } > salidas/scale_c_4096x16_hilo_1.sol.$t.$e 2>&1;
	done
done