#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <pthread.h>
#include <errno.h>
#include <libgen.h>
#include <math.h>
#include <time.h>

#define KLEN 128
#define VLEN 512
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MAXLEN 256
#define MAXLBL 64
#define ETC_MAX 64
#define MSG_LEN 256
#define LS_ITERATIONS 1000
#define LOAD_PERCENTILE 0.50

typedef struct task_lscape_t_ {
	int m;
	int *pb;
} task_lscape_t;

typedef struct {		/* induhvidual */
	int *sch;		/* task to machine assignment, the solution */
	double *mct;		/* machines' CT */
	double score;
} ind_t;

typedef struct {	/* parameters local to the instance */
	int tasks;		/* total nbr of tasks */
	int macs;		/* total nbr of machines */
	double *etc;		/* ETC */
	//double lda;		/* lambda */
	double lpc;		/* top and bottom percentile of machines */
} spar_t;

typedef struct entry_ {
	char key[KLEN];
	char val[VLEN];
} entry;



//HEADERS
void *xcalloc (size_t ecount, size_t esz);
int getaline (FILE *fh, char *line, int max);
double *readetc (const char *fn, int tsk, int mac);
char *val (char *key, entry * cfg, int len);
spar_t *newspar (int num_tasks, int num_machines, const char *fetc);
void fatal (FILE * s, const char *msg);
int ntaskm (ind_t * c, int m, spar_t * sp);
void deltsk (double *mct, int t, int m, int tsz, double *etc);
void addtsk (double *mct, int t, int m, int tsz, double *etc);
void mbll_k (ind_t * c, spar_t * sp);
void newind (ind_t * c, spar_t * sp);
void show_lscape(task_lscape_t *t);
void free_lscape(task_lscape_t *t);
int load_lscape(int machs, const char *fname, task_lscape_t *t, int tasks);
int load_prob (const char *pfx, int tasks, int machs, task_lscape_t *tl);
void svmind (ind_t *c, spar_t *sp, task_lscape_t *tl);
void *xmalloc (size_t sz);
void showind (ind_t * c, spar_t * sp);
void fitness (ind_t * c, spar_t * sp);
void comptime (ind_t * c, spar_t * sp);
void showpar (spar_t * s);
ind_t * best_individual (ind_t ** individuals, int length);


void showpar (spar_t * s)
{
	printf ("# tasks: %d\n", s->tasks);
	printf ("# machines: %d\n", s->macs);
	printf ("# load percentile: %.2f\n", s->lpc);
}

void showind (ind_t * c, spar_t * sp)
{
	printf ("schedule: ");
	for (int i = 0; i < sp->tasks; i++)
		printf ("%d ", c->sch[i]);
	printf ("\nmachines:\n");
	for (int i = 0; i < sp->macs; i++)
		printf ("mac %d: ct = %.1f\n", i, c->mct[i]);
	printf ("score: %.2f\n", c->score);
}

void fitness (ind_t * c, spar_t * sp)
{
	double mspan = 0;

	for (int m = 0; m < sp->macs; m++) {
		if (c->mct[m] > mspan)
			mspan = c->mct[m];
	}
	c->score = mspan;
}


void *xcalloc (size_t ecount, size_t esz)
{
	void *p = calloc (ecount, esz);
	if (p == NULL)
		fatal (stderr, "calloc");
	return p;
}

int getaline (FILE * h, char *line, int max)
{
	int c;
	int i = 0;

	memset (line, 0, max);
	while (1) {
		c = fgetc (h);
		if (c == '\n' || c == EOF)
			break;
		if (isspace (c))
			continue;
		if (i < max - 1)
			line[i++] = c;
	}
	if (i == 0 && c == EOF)
		return 1;
	return 0;
}

double *readetc (const char *fn, int tsk, int mac)
{
	FILE *fh;
	char line[ETC_MAX], msg[MSG_LEN];
	int i = 0, im, it, ne = tsk * mac;
	double dur, *etc;

	etc = xcalloc (ne, sizeof (*etc));
	fh = fopen (fn, "r");
	if (!fh) {
		perror (fn);
		fatal (stderr, "fopen");
	}
	while (i < ne && getaline (fh, line, ETC_MAX) == 0) {
		if (line[0] == '#' || line[0] == 0)
			continue;
		else if (sscanf (line, "%lf", &dur) != 1) {
			sprintf (msg, "error %s:%d \"%s\"\n", fn, i, line);
			fatal (stderr, msg);
		} else {
			im = i % mac;
			it = i / mac;
			etc[im * tsk + it] = dur;
			i++;
		}
	}
	fclose (fh);
	return etc;
}

char *val (char *key, entry * cfg, int len)
{
	int i = 0;
	while (i < len && strncmp (cfg[i].key, key, KLEN))
		i++;
	return i == len ? NULL : cfg[i].val;
}


spar_t *newspar (int num_tasks, int num_machines, const char *fetc)
{
	spar_t *par;

	par = xcalloc (1, sizeof (*par));
	//par->tasks = atoi (val ("tasks", cfg, lcfg));
	par->tasks=num_tasks;
	//par->macs = atoi (val ("machines", cfg, lcfg));
	par->macs=num_machines;
	par->etc = readetc (fetc, par->tasks, par->macs);
	//par->lda = atof (val ("lambda", cfg, lcfg));
	//par->lpc = atof (val ("load_percentile", cfg, lcfg));
	par->lpc = LOAD_PERCENTILE;
	return par;
}


void fatal (FILE * s, const char *msg)
{
	fprintf (s, "fatal \"%s\" error!\n", msg);
	exit (1);
}


struct ct_t {
	int id;
	double ct;
};

int cmpct (struct ct_t *a, struct ct_t *b)
{
	return (int) (a->ct - b->ct);
}

int ntaskm (ind_t * c, int m, spar_t * sp)
{
	int idx = rand () % sp->tasks;

	for (int i = 0; i < sp->tasks; i++, idx++) {
		if (idx >= sp->tasks)
			idx -= sp->tasks;
		if (c->sch[idx] == m)
			return idx;
	}
	fatal (stderr, "ntaskm: corrupt");
	return -1;
}

void deltsk (double *mct, int t, int m, int tsz, double *etc)
{
	mct[m] -= etc[m * tsz + t];
}

void addtsk (double *mct, int t, int m, int tsz, double *etc)
{
	mct[m] += etc[m * tsz + t];
}

void comptime (ind_t * c, spar_t * sp) 
{
	for (int i = 0; i < sp->macs; i++) {
		c->mct[i] = 0.0;
	}
	for (int i = 0; i < sp->tasks; i++)
		addtsk (c->mct, i, c->sch[i], sp->tasks, sp->etc);
}

void mbll_k (ind_t * c, spar_t * sp)
{
	int amx, t, ms, bestm = 0;
	struct ct_t m[sp->macs];
	double nscore, bestscore;

	/* how many machine to consider: */
	ms = sp->lpc * sp->macs;
	/* sort machines according on CT */
	for (int i = 0; i < sp->macs; i++) {
		m[i].id = i;
		m[i].ct = c->mct[i];
	}
	qsort (m, sp->macs, sizeof (struct ct_t),
	       (int (*)(const void *, const void *)) cmpct);
	amx = m[sp->macs - 1].id;
	t = ntaskm (c, amx, sp);
	bestscore = c->mct[amx];
	for (int j = 0; j < ms; j++) {
		/* nscore = m[j].ct + sp->etc[m[j].id + t * sp->macs]; */
		nscore = m[j].ct + sp->etc[m[j].id * sp->tasks + t];
		if (nscore < bestscore) {
			bestm = m[j].id;
			bestscore = nscore;
		}
	}
	if (bestscore < c->mct[amx]) {
		c->sch[t] = bestm;
		deltsk (c->mct, t, amx, sp->tasks, sp->etc);
		addtsk (c->mct, t, bestm, sp->tasks, sp->etc);
	}
}


void newind (ind_t * c, spar_t * sp)
{
	c->score = 0.0;
	c->mct = xcalloc (sp->macs, sizeof (*c->mct));
	c->sch = xcalloc (sp->tasks, sizeof (*c->sch));
	/* not required after calloc? */
	for (int i = 0; i < sp->macs; i++) {
		c->mct[i] = 0.0;
	}
	//if (pthread_rwlock_init (&c->lock, NULL) != 0)
	//	fatal (stderr, "init thread's rwlock");
}


void show_lscape(task_lscape_t *t) {
	for (int i = 0; i < t->m; ++i)
		printf("%.2d ", t->pb[i]);
	printf("\n");
}

void free_lscape(task_lscape_t *t) {
	free(t->pb);
}

/* Reads:
 * labels 0 2 1 3
 * 0 0.343075 0.0972486 0.229225 0.330452
 * or (!):
 * labels 0 2 1
 * 0 0.343075 0.0972486 0.229225
 */
int load_lscape(int machs, const char *fname, task_lscape_t *t, int tasks) {
	FILE *fd = fopen(fname, "r");
	if (! fd) {
		perror(fname);
		return 1;
	}
	size_t llen = MAXLEN;
	char *line = malloc(llen);
	size_t l = getline(&line, &llen, fd);
	if (l == -1) {
		free(line);
		return 1;
	}
	line[l - 1] = 0;	// remove the newline.
	const char delim[] = " ";
	char *token, *cp;
	cp = strdup(line);
	token = strtok(cp, delim);
	const char *pfix = "labels";
	if (strncmp(token, pfix, strlen(pfix))) {
		fprintf(stderr, "incorrect format in \"%s\"\n", line);
		free(line);
		free(cp);
		return 2;
	}
	int lbli[machs];
	int nlbl;
	for (nlbl = 0; (token = strtok(NULL, delim)); ++nlbl)
			lbli[nlbl] = (int)strtol(token, NULL, 10);
	free(cp);


	for (int i=0;i<tasks;++i){	
		l = getline(&line, &llen, fd);

		
		if (l == -1) {
			free(line);
			return 3;
		}
		line[l - 1] = 0;
		cp = strdup(line);
		token = strtok(cp, delim);
		//t[i].pb = malloc(machs * sizeof(int));
		//memset(t[i].pb, 0, machs * sizeof(int));
		int j;
		for (j = 0; j < nlbl && (token = strtok(NULL, delim)); ++j) 
			t[i].pb[lbli[j]] = (int)round((1000.0 * strtod(token, NULL)));
		if (nlbl != j) {
			fprintf(stderr, "incorrect format in \"%s\"\n", line);
			free(cp);
			free(line);
			return 4;
		}
		t[i].m = machs;
		//(t+i)->pb = pb;
	}
	free(cp);
	free(line);
	return 0;
}




void svmind (ind_t *c, spar_t *sp, task_lscape_t *tl)
{
	int r, sum, assigned;
	task_lscape_t *task;

	for (int i = 0; i < sp->tasks; i++) {
		task = tl+i;
		r = rand() % 1000;
		sum = 0;
		assigned = 0;
		for (int m=0; m < task->m; ++m) {
			//printf("%d\n",task->pb[m]);
			sum += task->pb[m];
			if (r < sum) {
				assigned = 1;
				c->sch[i] = m;
				break;
			} 
		}
		if (! assigned) {
			r = rand () % sp->macs;
			c->sch[i] = r;
		}
	}
}

void *xmalloc (size_t sz)
{
	void *p = malloc (sz);
	if (p == NULL)
		fatal (stderr, "malloc");
	return p;
}

ind_t * best_individual (ind_t** individuals, int length)
{
	int index_best=0;
	for (int i=0; i<length; ++i){
		if (individuals[i]->score< individuals[index_best]->score)
			index_best=i;
	}
	return individuals[index_best];
}


int main ( int argc, char **argv ) {
	if (argc < 6) {
		fprintf (stderr, "usage: %s <tasks> <machines> <ETC file> <model_path> <cores>",
			 argv[0]);
		exit (EXIT_FAILURE);
	}
	//Initialize RNG
	srand(time(NULL));
	rand();

	//Parse arguments and read problem instance
	spar_t *sp;
	char rname[MAXLEN];
	char fetc2 [MAXLEN];
	sprintf (fetc2, "savantSched/instances/%s",argv[3]);
	int num_tasks=atoi(argv[1]);
	int num_machines=atoi(argv[2]);
	sp = newspar (num_tasks, num_machines, (const char *) fetc2);

	//Parallel
	int num_cores=atoi(argv[5]);
	//EXPORT OMP_THREAD NUM
	char export_thread_num_cmd [MAXLEN];
	sprintf(export_thread_num_cmd, "export OMP_NUM_THREADS=%i\n",num_cores);
	//printf(export_thread_num_cmd);
	system(export_thread_num_cmd);

	//Scale
	char scale_cmd [MAXLEN];
	sprintf(scale_cmd,"./libsvm-3.21/svm-scale -r %s/range savantSched/instances_libsvm/%s.svm > scaled_instance\n",argv[4],argv[3]);
	printf(scale_cmd);
	system (scale_cmd);
	//Predict
	char predict_cmd [MAXLEN];
	sprintf(predict_cmd,"./libsvm-3.21/svm-predict -b 1 scaled_instance %s/model prediction >/dev/null 2>&1\n",argv[4]);
	printf (predict_cmd);
	system (predict_cmd);

	task_lscape_t *tlp = NULL;	
	sprintf(rname,"%s","prediction");
	tlp = malloc (sp->tasks * sizeof(*tlp));
	for (int i=0;i<num_tasks;++i){
		tlp[i].pb = malloc(num_machines * sizeof(int));
		memset(tlp[i].pb, 0, num_machines * sizeof(int));
	}

	// Read the probability vectors for each task, based on some trained SVM:
	if (load_lscape (sp->macs, rname, tlp, sp->tasks)) {
		exit (EXIT_FAILURE);
	}

	//DEBUG Probability load
	// for (int i=0;i<num_tasks;++i){
	// 	for (int j=0;j<num_machines;++j)
	// 		printf("%i ",tlp[i].pb[j]);
	// 	printf("\n");
	// }
	//Create threads for LS
	ind_t ** results_individual = malloc(num_cores*sizeof(ind_t*));

	//OPENMP
	#pragma omp parallel for
	for (int thread=0; thread<num_cores; ++thread){
		results_individual[thread]=xmalloc (sizeof (ind_t));
		newind (results_individual[thread], sp);
		svmind(results_individual[thread], sp, tlp);
		comptime(results_individual[thread],sp);
		for (int i = 0; i < LS_ITERATIONS; i++)
			mbll_k(results_individual[thread],sp);
		comptime(results_individual[thread],sp);
		fitness(results_individual[thread], sp);
	}
	showind(best_individual(results_individual, num_cores), sp);

	
	return 0;
}