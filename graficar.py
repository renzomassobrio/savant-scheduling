import os
import matplotlib.pyplot as plt 
import numpy as np


cores=range(1,273,8)
executions=range(1,11)
solution_path="salidas"

instances=["new_c_512x16_hihi_1","scale_c_4096x16_hihi_1"]

for instance in instances:
	scores=[]
	times=[]
	for core in cores:
		scores_core=[]
		times_core=[]
		for execution in executions:
			file=open("%s/%s.sol.%d.%d"%(solution_path,instance,core,execution))
			for line in file.readlines():
				if line.startswith("score:"):
					scores_core.append(float(line[7:]))
				if line.startswith("real"):
					time_str=line[5:]
					minutes=float(time_str.split("m")[0])
					seconds=float(time_str.split("m")[1][:-2])
					times_core.append(minutes*60+seconds)
			file.close()
		scores.append(scores_core)
		times.append(times_core)

		# print ("%d & "%(core-1)),
		# print ("%.2f & "%(min(scores_core))),
		# print ("%.2f & "%(np.mean(np.array(scores_core)))),
		# print ("%.2f & "%(np.median(np.array(scores_core)))),
		# print ("%.2f & "%(np.std(np.array(scores_core)))),
		# print ("%.2f & "%(min(times_core))),
		# print ("%.2f & "%(np.mean(np.array(times_core)))),
		# print ("%.2f & "%(np.median(np.array(times_core)))),
		# print ("%.2f \\\\"%(np.std(np.array(times_core))))

	#Resultados Min-Min
	tasks,machines=instance.split("_")[2].split("x")
	os.system('./compute_score %s %s %s.dat resultadosMinMin/%s.dat.min > tmp'%(tasks,machines,instance, instance))
	makespan_minmin=float(open('tmp', 'r').read())


	plt.clf()

	fig = plt.figure(1)
	ax = fig.add_subplot(111)
	bp = ax.boxplot(scores, 1)
	ax.axhline(y=makespan_minmin, label="MinMin")
	ax.set_xticklabels([1]+[core-1 for core in cores[1:]])
	ax.set_xlabel("threads")
	ax.set_ylabel("makespan")
	plt.xticks(rotation=90)
	#ax.set_title("%s"%instance)
	fig.savefig('graficas/%s_makespan.png'%instance,bbox_inches='tight')

	plt.clf()

	fig = plt.figure(1)
	ax = fig.add_subplot(111)
	bp = ax.boxplot(times, 1)
	ax.set_xticklabels([1]+[core-1 for core in cores[1:]])
	ax.set_xlabel("threads")
	ax.set_ylabel("time (s)")
	#ax.set_title("%s"%instance)
	plt.xticks(rotation=90)
	fig.savefig('graficas/%s_time.png'%instance,bbox_inches='tight')


